#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int valido(char *);
int validonum(int );

int main(){
	float count = 0;
        int epvieja = 0;
        int epjoven = 131;
        char pvieja[20];
        char pjoven[20];

	for(int i=1; i<11; i++){

        printf("Persona %d:\n", i);

        char nombre[20];
        printf("Escribe el nombre: ");
        scanf("%s", nombre);
        while(valido(nombre)==0){
            printf("Escribe el nombre: ");
            scanf("%s", nombre);
        }

        int edad;
        printf("Escribe la edad: ");
        scanf("%d", &edad);
        validonum(edad);

        count += edad;

        if(edad > epvieja){
            epvieja = edad;
            strcpy(pvieja, nombre);
        }

        if(edad < epjoven){
            epjoven = edad;
            strcpy(pjoven,nombre);
        }

	}

	printf("El promedio de edades es: %.2f\n", (count/10));
	printf("La persona mas vieja es: %s\n", pvieja);
	printf("La persona mas joven es: %s\n", pjoven);
}

int valido(char *c)
{
     if ((c[0]>='A' && c[0]<='Z'))
     {
          for(int i=1; i<20; i++ )
          {
	     if( c[i] == 0)
	          break;
             if( !(c[i]>='a' && c[i]<='z'))
                  return 0;
           }
	   return 1;
     }
     else
          return 0;

}

int validonum(int num){
     while(!(num>=0 && num<=130)){
	  printf("Escriba la edad: ");
	  scanf("%d", &num);
     }
}
